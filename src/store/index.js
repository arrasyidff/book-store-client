import Vue from 'vue'
import Vuex, { createLogger } from 'vuex'
import wishlist from './modules/wishlist'
import cart from './modules/cart'
import book from './modules/book'
import category from './modules/category'
import author from './modules/author'
import publisher from './modules/publisher'
import shipment from './modules/shipment'
import address from './modules/address'
import cost from './modules/cost'
import order from './modules/order'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createLogger],
  modules: {
    wishlist,
    cart,
    book,
    category,
    author,
    publisher,
    shipment,
    address,
    cost,
    order,
    user
  }
})
