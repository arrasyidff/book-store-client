import { instance as Axios } from '../../config/axiosInstance'

export default {
  namespaced: true,
  state: {
    jne: [],
    tiki: [],
    pos: []
  },
  mutations: {
    setJne (state, payload) {
      state.jne = payload
    },
    setTiki (state, payload) {
      state.tiki = payload
    },
    setPos (state, payload) {
      state.pos = payload
    }
  },
  actions: {
    fetchCourier (context, { origin, destination, courier, weight }) {
      return new Promise((resolve, reject) => {
        Axios({
          url: 'get-cost',
          method: 'POST',
          data: {
            origin,
            destination,
            courier,
            weight
          }
        })
          .then(resp => {
            if (courier === 'jne') {
              context.commit('setJne', resp.data)
            } else if (courier === 'pos') {
              context.commit('setPos', resp.data)
            } else if (courier === 'tiki') {
              context.commit('setTiki', resp.data)
            }
            resolve(resp.data)
          })
          .catch(err => [
            reject(err)
          ])
      })
    }
  }
}
