import { instance as Axios } from '../../config/axiosInstance'

export default {
  namespaced: true,
  state: {
    total: 0,
    carts: []
  },
  mutations: {
    setTotal (state, payload) {
      state.total = payload
    },
    setCarts (state, payload) {
      state.carts = payload
    }
  },
  actions: {
    async fetchCount (context) { //eslint-disable-line
      try {
        const resp = await Axios.get('cart?key=count')
        context.commit('setTotal', resp.data.total_data)
      } catch (error) {
        context.commit('setTotal', 0)
        console.warn(error.response)
      }
    },
    async addCart (context, { bookId, quantity }) { //eslint-disable-line
      try {
        const resp = await Axios.post(`cart/${bookId}`, {
          quantity
        })
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async fetchCarts (context) {
      try {
        const resp = await Axios.get('cart')
        context.commit('setCarts', resp.data.data)
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async incrementQuantity (context, { bookId }) {
      try {
        const resp = await Axios.patch(`cart/${bookId}/increment`)
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async decrementQuantity (context, { bookId }) {
      try {
        const resp = await Axios.patch(`cart/${bookId}/decrement`)
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async deteleItem (context, { bookId }) {
      try {
        const resp = await Axios.delete(`cart/${bookId}`)
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async deteleItems (context, { booksId }) {
      try {
        const resp = await Axios.delete('carts-delete', {
          data: {
            booksId
          }
        })
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async checkedItem (context, { bookId }) {
      try {
        const resp = await Axios.patch(`cart/${bookId}/selected`)
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async checkedAllItem (context, { isSelected }) {
      try {
        const resp = await Axios.patch(`cart/${null}/selected?key=all`, {
          is_selected: isSelected
        })
        return resp.data
      } catch (error) {
        throw error
      }
    }
  }
}
