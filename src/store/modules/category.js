import { instance as Axios } from '../../config/axiosInstance'

export default {
  namespaced: true,
  state: {
    categories: []
  },
  mutations: {
    setCategories (state, payload) {
      state.categories = payload
    }
  },
  actions: {
    fetchCategories (context) { //eslint-disable-line
      Axios({
        url: `categories`, //eslint-disable-line
        method: 'GET'
      })
        .then(resp => {
          context.commit('setCategories', resp.data.response)
        })
        .catch(err => {
          console.warn(err.response)
        })
    }
  }
}
