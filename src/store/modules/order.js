import { instance as Axios } from '../../config/axiosInstance'

export default {
  namespaced: true,
  state: {
    orders: [],
    order: null
  },
  mutations: {
    setOrders (state, payload) {
      state.orders = payload
    },
    setOrder (state, payload) {
      state.order = payload
    }
  },
  actions: {
    checkout (context, { payment_method, total_price, courier, cost, user_address_id, books }) { //eslint-disable-line
      return new Promise((resolve, reject) => {
        Axios({
          url: 'checkout', //eslint-disable-line
          method: 'POST',
          data: {
            payment_method,
            total_price,
            courier,
            cost,
            user_address_id,
            books
          }
        })
          .then(resp => {
            resolve(resp)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    async fetchOrders (context) { //eslint-disable-line
      try {
        const resp = await Axios.get('/orders/user')
        context.commit('setOrders', resp.data.data)
      } catch (error) {
        console.log(error.response)
      }
    },
    async fetchOrder (context, { orderId }) {
      try {
        const order = await Axios.get('orders/' + orderId)
        context.commit('setOrder', order.data.data)
      } catch (error) {
        console.log(error)
      }
    }
  },
  getters: {
    getDoneOrders (state) {
      return state.orders.filter(order => order.order_status === 'settlement')
    },
    getPendingOrders (state) {
      return state.orders.filter(order => order.order_status === 'pending')
    },
    getExpiredOrders (state) {
      return state.orders.filter(order => order.order_status === 'expire')
    }
  }
}
