import { instance as Axios } from '../../config/axiosInstance'

export default {
  namespaced: true,
  state: {
    shipments: []
  },
  mutations: {
    setShipments (state, payload) {
      state.shipments = payload
    }
  },
  actions: {
    fetchShipments (context) { //eslint-disable-line
      return new Promise((resolve, reject) => {
        Axios({
          url: 'shipment', //eslint-disable-line
          method: 'GET'
        })
          .then(resp => {
            context.commit('setShipments', resp.data.data)
            resolve(resp.data.data)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    async createShipment (context, { items, cbSuccess, cbError }) { //eslint-disable-line
      try {
        const resp = await Axios.post('shipment', {
          items
        })
        return resp.data.data
      } catch (error) {
        throw error
      }
    },
    fetchProvince (context, { cbSuccess, cbError }) {
      Axios({
        url: 'province',
        method: 'GET'
      })
        .then(resp => {
          cbSuccess(resp.data.data)
        })
        .catch(err => {
          cbError(err)
        })
    },
    fetchCity (context, { provinceId, cbSuccess, cbError }) {
      Axios({
        url: `city?province=${provinceId}`,
        method: 'GET'
      })
        .then(resp => {
          cbSuccess(resp.data.data)
        })
        .catch(err => {
          cbError(err)
        })
    }
  }
}
