import { instance as Axios } from '../../config/axiosInstance'
import qs from 'qs'

export default {
  namespaced: true,
  state: {
    books: [],
    bestSeller: [],
    book: null
  },
  mutations: {
    setBooks (state, payload) {
      state.books = payload
    },
    setBestSeller (state, payload) {
      state.bestSeller = payload
    },
    setBook (state, payload) {
      state.book = payload
    }
  },
  getters: {
    titleBook (state, getters) {
      console.log()
      if (state.book) {
        return state.book.title
      }
      return ''
    }
  },
  actions: {
    async fetchBooks (context, { limit, userId, query }) { //eslint-disable-line
      try {
        const localQuery = { ...query, limit }
        if (userId) {
          localQuery.user_id = userId
        }
        const queryString = qs.stringify(localQuery)
        const resp = await Axios.get(`books?${queryString}`)
        context.commit('setBooks', resp.data.data.data)
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async fetchBestSeller (context, { userId, limit }) { //eslint-disable-line
      try {
        const query = {
          value: 'bestSeller',
          limit
        }
        if (userId) {
          query.user_id = userId
        }
        const queryString = qs.stringify(query)
        const resp = await Axios.get(`books?${queryString}`)
        context.commit('setBestSeller', resp.data.data.data)
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async fetchOneBook (context, { bookId, userId }) {
      try {
        let queryString = null
        if (userId) {
          queryString = qs.stringify({ user_id: userId })
        }
        const resp = await Axios.get(`book/${bookId}?${queryString}`)
        context.commit('setBook', resp.data.data)
        return resp.data
      } catch (error) {
        throw error // eslint-disable-line
      }
    },
    fetchBooksName (context, { title, cbSuccess, cbError }) {
      Axios({
        url: `book-name/?title=${title}`, //eslint-disable-line
        method: 'GET'
      })
        .then(resp => {
          cbSuccess(resp.data.data)
        })
        .catch(err => {
          cbError(err.response)
        })
    }
  }
}
