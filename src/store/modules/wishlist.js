import { instance as Axios } from '../../config/axiosInstance'

export default {
  namespaced: true,
  state: {
    total: 0,
    wishlist: []
  },
  mutations: {
    setTotal (state, payload) {
      state.total = payload
    },
    setWishlists (state, payload) {
      state.wishlist = payload
    }
  },
  actions: {
    async fetchCount (context, payload) { //eslint-disable-line
      try {
        const resp = await Axios.get('wishlist?key=count')
        context.commit('setTotal', resp.data.data.total_data)
      } catch (error) {
        context.commit('setTotal', 0)
        console.warn(error.response)
      }
    },
    async addWishlist (context, { bookId }) { //eslint-disable-line
      try {
        const resp = await Axios.post('wishlist', {
          book_id: bookId
        })
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async fetchWishlist (context) {
      try {
        const resp = await Axios.get('wishlist')
        console.log(resp)
        const result = resp.data.data.map(item => {
          return { ...item.Book, is_wishlist: item.is_wishlist}
        })
        context.commit('setWishlists', result)
        return resp.data
      } catch (error) {
        throw error
      }
    }
  }
}
