import { instance as Axios } from '../../config/axiosInstance'

export default {
  namespaced: true,
  state: {
    authors: []
  },
  mutations: {
    setAuthors (state, payload) {
      state.authors = payload
    }
  },
  actions: {
    fetchAuthors (context) { //eslint-disable-line
      Axios({
        url: `authors_name?limit=30`, //eslint-disable-line
        method: 'GET'
      })
        .then(resp => {
          context.commit('setAuthors', resp.data.response)
        })
        .catch(err => {
          console.warn(err.response)
        })
    }
  }
}
