import { instance as Axios } from '../../config/axiosInstance'

export default {
  namespaced: true,
  state: {
    publishers: []
  },
  mutations: {
    setPublishers (state, payload) {
      state.publishers = payload
    }
  },
  actions: {
    fetchPublishers (context) { //eslint-disable-line
      Axios({
        url: `publishers`, //eslint-disable-line
        method: 'GET'
      })
        .then(resp => {
          context.commit('setPublishers', resp.data.response)
        })
        .catch(err => {
          console.warn(err.response)
        })
    }
  }
}
