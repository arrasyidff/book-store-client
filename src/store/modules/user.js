import { instance as Axios } from '../../config/axiosInstance'

export default {
  namespaced: true,
  state: {
    user: {
      full_name: '',
      date_of_birth: '',
      email: '',
      gender: '',
      phone_number: '',
      image: ''
    },
    userImage: null
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    },
    setUserImage (state, payload) {
      state.userImage = payload
    }
  },
  actions: {
    async fetchUser (context, { userId }) {
      try {
        const resp = await Axios.get(`user/${userId}`)
        context.commit('setUser', resp.data.data)
        context.commit('setUserImage', resp.data.data.image)
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async updateUser (context, { userId, payload: { full_name, date_of_birth, gender, email, phone_number } }) {
      try {
        const resp = await Axios.patch(`user/${userId}`, {
          full_name,
          date_of_birth,
          gender,
          email,
          phone_number
        })
        return resp.data
      } catch (error) {
        throw error
      }
    }
  }
}
