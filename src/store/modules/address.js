import { instance as Axios } from '../../config/axiosInstance'

export default {
  namespaced: true,
  state: {
    addresses: []
  },
  mutations: {
    setAddresses (state, payload) {
      state.addresses = payload
    }
  },
  actions: {
    async fetchAddresses (context) {
      try {
        const resp = await Axios.get('address')
        context.commit('setAddresses', resp.data.data)
      } catch (error) {
        throw error        
      }
    },
    async createAddress (context, { payload }) { //eslint-disable-line
      try {
        const resp = await Axios.post('address', {
          receiver: payload.receiver,
          phone_number: payload.phone_number,
          province: payload.province,
          province_id: payload.province_id,
          city: payload.city,
          city_id: payload.city_id,
          postal_code: payload.postal_code,
          detail_address: payload.detail_address,
          is_main_address: payload.is_main_address
        })

        return resp.data
      } catch (error) {
        throw error
      }
    },
    async updateIsMainAddress (context, { addressId }) {
      try {
        const resp = await Axios.patch('address/' + addressId)
        return resp.data
      } catch (error) {
        throw error        
      }
    },
    async updateAddress (context, { addressId, receiver, phone_number, province, province_id, city, city_id, postal_code, detail_address, is_main_address }) {
      console.log(addressId, receiver, phone_number, province, province_id, city, city_id, postal_code, detail_address, is_main_address)
      try {
        const resp = await Axios.put('address/' + addressId, {
          receiver,
          phone_number,
          province,
          province_id,
          city,
          city_id,
          postal_code,
          detail_address,
          is_main_address
        })
        return resp.data
      } catch (error) {
        throw error
      }
    },
    async deleteAddress (context, { addressId }) {
      try {
        const resp = await Axios.delete('address/' + addressId)
        return resp
      } catch (error) {
        throw error        
      }
    }
  },
  getters: {
    getMainAddress (state) {
      if (state.addresses.length > 0) {
        const address = state.addresses.find(el => el.is_main_address === true)
        if (!address) {
          return state.addresses[0]
        }
        return address
      } else {
        return null
      }
    },
    getAddress: (state) => (addressId) => {
      return state.addresses.find(el => el.id === addressId)
    }
  }
}
