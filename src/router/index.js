import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Home from '../views/Home.vue'
import Books from '../views/Books.vue'
import Detail from '../views/Detail.vue'
import Cart from '../views/Cart.vue'
import Wishlist from '../views/Wishlist.vue'
import Checkout from '../views/Checkout.vue'
import Transactions from '../views/Transactions.vue'
import DetailTransaction from '../views/DetailTransaction.vue'
import Setting from '../views/Setting.vue'
import NotFound from '../views/NotFound.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem('access_token')) {
        next({ name: 'Home' })
      } else {
        next()
      }
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem('access_token')) {
        next({ name: 'Home' })
      } else {
        next()
      }
    }
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/books',
    name: 'Books',
    component: Books
  },
  {
    path: '/:book_id/:title',
    name: 'Books-Detail',
    component: Detail
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart,
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem('access_token')) {
        next({ name: 'Login' })
      } else {
        next()
      }
    }
  },
  {
    path: '/checkout',
    name: 'Checkout',
    component: Checkout,
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem('access_token')) {
        next({ name: 'Login' })
      } else {
        next()
      }
    }
  },
  {
    path: '/wishlist',
    name: 'Wishlist',
    component: Wishlist,
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem('access_token')) {
        next({ name: 'Login' })
      } else {
        next()
      }
    }
  },
  {
    path: '/transactions',
    name: 'Transactions',
    component: Transactions,
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem('access_token')) {
        next({ name: 'Login' })
      } else {
        next()
      }
    }
  },
  {
    path: '/detail/transaction/:id',
    name: 'Detail Transaction',
    component: DetailTransaction,
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem('access_token')) {
        next({ name: 'Login' })
      } else {
        next()
      }
    }
  },
  // {
  //   path: '/linkedin',
  //   name: 'Linkedin',
  //   beforeEnter (to, from, next) {
  //     window.open('https://www.linkedin.com/in/arrasyid-fadel-fatonsyah/', '_blank')
  //   }
  // },
  // {
  //   path: '/github',
  //   name: 'Github',
  //   beforeEnter (to, from, next) {
  //     window.open('https://www.linkedin.com/in/arrasyid-fadel-fatonsyah/', '_blank')
  //   }
  // },
  {
    path: '/payment-simulator',
    name: 'Payment Simulator',
    beforeEnter (to, from, next) {
      window.open('https://simulator.sandbox.midtrans.com/mandiri/bill/index', '_blank')
    }
  },
  {
    path: '/setting',
    name: 'Setting',
    component: Setting,
    beforeEnter: (to, from, next) => {
      if (!localStorage.getItem('access_token')) {
        next({ name: 'Login' })
      } else {
        next()
      }
    }
  },
  {
    path: '*',
    name: 'Page Not Found',
    component: NotFound
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export default router
