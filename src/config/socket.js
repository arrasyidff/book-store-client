import io from 'socket.io-client'
import { baseURL } from '@/config/axiosInstance.js'

export default class SocketIo {
  constructor () {
    this.socket = io(baseURL)
  }

  connect () {
    this.socket.on('connect', () => {
      console.log(this.socket.id, 'masuk')
    })
  }

  emitter (event, payload) {
    this.socket.emit(event, payload)
  }

  updateStatusTransaction (on) {
    this.socket.on('refreshPageAfterTransaction', (payload) => {
      console.log(payload)
      on(payload)
    })
  }
}
