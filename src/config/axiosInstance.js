import axios from 'axios'

const baseURL = 'http://localhost:3000/'
// const baseURL = 'https://book-store-arfafa.herokuapp.com/'

const instance = axios.create({
  baseURL
})

const setAccessToken = (token) => {
  instance.defaults.headers.common.access_token = token
}

instance.defaults.headers.common.access_token = localStorage.getItem('access_token')

export { instance, setAccessToken, baseURL }
