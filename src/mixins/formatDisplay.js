export default {
  methods: {
    formatPrice (value) {
      if (value !== undefined || value !== null) {
        value = +value
        const reverseValue = value.toString().split('').reverse().join('')
        let price = reverseValue.match(/\d{1,3}/g)
        price = price.join('.').split('').reverse().join('')
        return price
      } else {
        return '...'
      }
    },
    formatAddress (detailAddress, province, city, postalCode) {
      return `${detailAddress}, ${province}, ${city}, ${postalCode}`
    },
    formatDaysCourier (days) {
      if (days.split(' ').length > 1) {
        let tempDays = ''
        days.split(' ').forEach(el => {
          if (typeof +el === 'number' && +el) {
            tempDays = el
          }
        })
        days = tempDays + ' days'
      } else if (days.split('-').length > 1) {
        days = days.split('-').join(' - ') + ' days'
      } else {
        days = days + ' days'
      }
      return days
    },
    formatDate (value) {
      const date = new Date(value)
      const day = date.toLocaleString('default', { day: '2-digit' })
      const month = date.toLocaleString('default', { month: 'short' })
      const year = date.toLocaleString('default', { year: 'numeric' })
      return day + '-' + month + '-' + year
    }
  }
}
